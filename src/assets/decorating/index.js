import crown from './crown.gif';
import desktop from './desktop.jpg';
import draw from './draw.png';
import foul from './foul.png';
import set from './set.png';
import stack from './stack.png';

export {
    crown,
    desktop,
    draw,
    foul,
    set,
    stack
}
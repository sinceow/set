import cardClick from "./card-click.mp3"
import dealCard from "./deal-card.wav"
import foul from "./foul.mp3"
import set from "./set.mp3"
import setClick from "./set-click.wav"
import cheerful from "./cheerful.mp3"
import drawGame from "./draw-game.mp3"

export {
    cardClick,
    dealCard,
    foul,
    set,
    setClick,
    cheerful,
    drawGame
}
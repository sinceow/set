import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
import path from "path"

// https://vitejs.dev/config/
export default defineConfig({
    base: '/set/',
    plugins: [vue()],
    resolve: {
    },
    assetsInclude: ['src/assets/set/*.png']
})
